package com.pedro.demo.model;




import javax.persistence.*;


@Entity
public class Book{


    @Id
    @Column(columnDefinition = "varchar(36) DEFAULT uuid_generate_v4()::varchar", name = "book_id")
    private String id;

    @Column(length = 100)
    String title;

    @Column(length = 1000)
    String description;

    @OneToOne
    Author author;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
