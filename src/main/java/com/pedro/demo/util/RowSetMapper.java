package com.pedro.demo.util;


import io.vertx.sqlclient.RowSet;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Slf4j
public class RowSetMapper<T> {
    Object join = null;
    List joins = new ArrayList();
    T dto;
   public List<T> map(RowSet resultSet, Class<T> clazz){
        List<Field> fields = Arrays.asList(clazz.getDeclaredFields());
        for(Field field: fields) {
            field.setAccessible(true);
        }

        List<T> list = new ArrayList<>();
        resultSet.forEach(r ->  {

            dto = null;
            try {
                dto = clazz.getConstructor().newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }




            if(clazz.isAnnotationPresent(OneToOne.class)) {
                for (Field field : fields) {

                    if (field.isAnnotationPresent(OneToOne.class)) {
                        RowSetMapper rowMapper = new RowSetMapper<>();
                        join = rowMapper.map(resultSet, field.getType()).get(0);
                        try {
                            field.set(dto, field.getType().getConstructor(field.getType()).newInstance(join));
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        }
                        break;
                    } else {
                        String name = field.getAnnotation(Column.class).name();
                        if (name.isEmpty()) {
                            name = field.getName();
                        }
                        try {
                            Object value = r.getValue(name);
                            if (value.getClass() == Long.class) {
                                long a = (long) value;
                                field.setLong(dto, a);
                            } else {
                                field.set(dto, field.getType().getConstructor(field.getType()).newInstance(value));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            } else if(clazz.isAnnotationPresent(OneToMany.class)){
                for (Field field : fields) {

                    if (field.isAnnotationPresent(OneToOne.class)) {
                        RowSetMapper rowMapper = new RowSetMapper<>();
                        join = rowMapper.map(resultSet, field.getType());
                        try {
                            field.set(dto, field.getType().getConstructor(field.getType()).newInstance(join));
                        } catch (InstantiationException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        } catch (InvocationTargetException e) {
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            e.printStackTrace();
                        }
                        break;
                    } else {
                        String name = field.getAnnotation(Column.class).name();
                        if (name.isEmpty()) {
                            name = field.getName();
                        }
                        try {
                            Object value = r.getValue(name);
                            if (value.getClass() == Long.class) {
                                long a = (long) value;
                                field.setLong(dto, a);
                            } else {
                                field.set(dto, field.getType().getConstructor(field.getType()).newInstance(value));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }
            list.add(dto);

        });
    return list;
    }

}


