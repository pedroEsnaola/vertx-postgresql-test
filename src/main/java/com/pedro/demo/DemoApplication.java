package com.pedro.demo;

import io.vertx.core.Vertx;
import io.vertx.core.WorkerExecutor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@SpringBootApplication
@Configuration
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    Vertx vertx = Vertx.vertx();

    @Bean
    @Scope("singleton")
    WorkerExecutor workerExecutor(){
        return vertx.createSharedWorkerExecutor("persistence-worker", 2);
    }

    @Bean
    @Scope("singleton")
    Vertx vertx(){
        return vertx;
    }
}
