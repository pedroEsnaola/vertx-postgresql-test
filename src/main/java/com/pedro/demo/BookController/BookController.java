package com.pedro.demo.BookController;

import com.pedro.demo.model.Author;
import com.pedro.demo.model.Book;
import com.pedro.demo.service.BookService;
import com.zandero.rest.annotation.Get;
import io.vertx.core.Future;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/book")
@Component
@Slf4j
public class BookController {

    BookService service;


    public BookController(BookService service) {
        this.service = service;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Book saveBook(Book book){
        service.saveBook(book);
        return book;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Future<List<Book>> findBook(){
        Future<List<Book>> p = Future.future();
        service.findAllBooks(p);
        return p;
    }

    @Get("/id/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Future<Book> findById(@PathParam("id") String id){
      Future<Book> future = Future.future();
      service.findById(id, future);
      return future;
//        Book book = new Book();
//        Author author = new Author();
//        author.setName("Pedro");
//        book.setAuthor(author);
//        book.setDescription("Alo");
//        book.setTitle("A");
//        book.setId("any");
//        return book;
    }

    @Get("/description/{description}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Future<List<Book>> findByDescription(@PathParam("description") String description){
        Future<List<Book>> booksPromise = Future.future();
        service.findBy("description", description, booksPromise);
        return booksPromise;
    }




}

