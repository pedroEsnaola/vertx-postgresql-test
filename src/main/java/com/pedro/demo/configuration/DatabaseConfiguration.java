package com.pedro.demo.configuration;

import io.vertx.pgclient.PgConnectOptions;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.PoolOptions;
import io.vertx.sqlclient.RowSet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class DatabaseConfiguration {

    PgConnectOptions connectOptions = new PgConnectOptions()
            .setPort(5432)
            .setHost("localhost")
            .setDatabase("book")
            .setUser("postgres")
            .setPassword("")
            .setSsl(false);

    // Pool options
    PoolOptions poolOptions = new PoolOptions()
            .setMaxSize(20);

    // Create the client pool
    @Bean
    @Scope("singleton")
    public PgPool pgPool(){
    return PgPool.pool(connectOptions, poolOptions);
    }


}
