package com.pedro.demo.service;

import com.pedro.demo.model.Author;
import com.pedro.demo.util.RowSetMapper;
import io.vertx.core.Future;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.SqlConnection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class AuthorService {

    PgPool client;
    RowSetMapper<Author> rowSetMapper = new RowSetMapper<>();

    public AuthorService(PgPool client, RowSetMapper<Author> rowSetMapper) {
        this.client = client;
        this.rowSetMapper = rowSetMapper;
    }

    public void findAll(Future<List<Author>> promise){
        client.getConnection(connectionTry -> {
            if(connectionTry.succeeded()){
                SqlConnection conn = connectionTry.result();
                conn.query("SELECT * FROM author  JOIN book ON author.author_id = book.author_author_id", res -> {

                });
            }else{
                log.error(connectionTry.cause().getLocalizedMessage());
            }
        });
    }
}
