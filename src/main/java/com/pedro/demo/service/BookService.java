package com.pedro.demo.service;

import com.pedro.demo.model.Book;
import com.pedro.demo.util.RowSetMapper;
import io.vertx.core.Future;
import io.vertx.pgclient.PgPool;
import io.vertx.sqlclient.SqlConnection;
import io.vertx.sqlclient.Transaction;
import io.vertx.sqlclient.Tuple;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class BookService {

    private PgPool client;
    RowSetMapper<Book> rowSetMapper = new RowSetMapper<>();

    public BookService(PgPool client) {

        this.client = client;
    }

    public void findById(String id, Future<Book> fut){
        if(id == null){
            throw new RuntimeException("Id must be not null");
        }
        client.getConnection(connectionTry -> {
            if(connectionTry.succeeded()){
                SqlConnection conn = connectionTry.result();
                conn.preparedQuery("SELECT * FROM book where author_id = $1 ", Tuple.of(id), res -> {
                    if(res.succeeded()){
                        Book book = rowSetMapper.map(res.result(), Book.class).get(0);
                        fut.complete(book);
                    }else{
                        log.error("Erro ao executar a busca " + res.cause().getLocalizedMessage());
                    }
                });
            }else{
                log.error("Erro ao conectar com o Banco de Dados: " + connectionTry.cause().getLocalizedMessage());
            }
        });
    }

    public void saveBook(Book book){
        client.getConnection(con -> {
            if(con.succeeded()){
                SqlConnection sql =  con.result();
                Transaction tx = sql.begin();
                sql.preparedQuery("INSERT INTO book(description, title) values ($1, $2)", Tuple.of(book.getDescription(), book.getTitle()), res -> {
                    if(res.succeeded()){
                        tx.commit();
                        sql.close();
                        log.info("Persisted book");
                    }else{
                        log.error(res.cause().getMessage());
                        tx.rollback();
                        sql.close();
                    }
                });
            }else{
                log.error(con.cause().getMessage());
            }

        });
    }

    public void findAllBooks(Future<List<Book>> future){
        client.getConnection(connectionTry -> {
            if(connectionTry.succeeded()){
                SqlConnection conn = connectionTry.result();
                conn.query("SELECT * FROM book INNER JOIN author ON author.author_id = book.author_author_id", res-> {
                    if(res.succeeded()){
                        List<Book> books = rowSetMapper.map(res.result(), Book.class);
                        future.complete(books);
                        conn.close();
                    }else{
                        log.error("Erro ao buscar todos os Livros:" +  res.cause().getLocalizedMessage());
                    }
                });
            }
        });
    }

    public void findBy(String field ,String value ,Future<List<Book>> booksPromise){
        client.getConnection(connectionTry-> {
            if(connectionTry.succeeded()){
                SqlConnection conn = connectionTry.result();
                conn.preparedQuery("SELECT * FROM book where description = $1", Tuple.of(value), res -> {
                    if(res.succeeded()){
                        List<Book> books = rowSetMapper.map(res.result(), Book.class);
                        booksPromise.complete(books);
                        conn.close();

                    }else{
                        booksPromise.complete(null);
                        log.error("Erro ao executar busca");
                    }
                });


            }else{
                log.error("Erro ao adquirir conexao:" + connectionTry.cause().getLocalizedMessage());
            }
        });
    }

}
