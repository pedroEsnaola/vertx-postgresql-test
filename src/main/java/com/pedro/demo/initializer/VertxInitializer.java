package com.pedro.demo.initializer;

import com.pedro.demo.BookController.BookController;
import com.zandero.rest.RestRouter;
import io.vertx.core.*;
import io.vertx.ext.web.Router;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Configuration
public class VertxInitializer implements ApplicationListener<ContextRefreshedEvent> {
    BookController controller;

    Vertx vertx;

    public VertxInitializer(BookController controller, Vertx vertx) {
        this.controller = controller;
        this.vertx = vertx;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {


        Router bookRouter = RestRouter.register(vertx, controller);

        vertx.deployVerticle(() -> new AbstractVerticle() {
            @Override
            public void start(Future<Void> startFuture) throws Exception {

                vertx.createHttpServer()
                        .requestHandler(bookRouter)
                        .listen(8081);
            }
        }, new DeploymentOptions().setInstances(6));

    }


}
